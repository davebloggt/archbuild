FROM archlinux/base as aur

COPY mirrorlist /etc/pacman.d/mirrorlist

RUN pacman --noconfirm --needed -Syu base-devel git

RUN useradd -m build
RUN echo "build ALL=(ALL) NOPASSWD:ALL" >>/etc/sudoers

USER build
WORKDIR /home/build

FROM aur as build

RUN sudo sed 's/pkg\.tar\.zst/pkg.tar/' -i /etc/makepkg.conf
RUN git clone https://aur.archlinux.org/yay.git && cd yay && makepkg --noconfirm -s

FROM aur

COPY --from=build /home/build/yay/*.pkg.tar /home/build/
RUN sudo pacman --noconfirm -U /home/build/*.pkg.tar && rm /home/build/*.pkg.tar

RUN yay --noconfirm -S python python-pyaml python-jinja

